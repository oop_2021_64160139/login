import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoginStore = defineStore("login", () => {
  const LoginName = ref("");
  const isLogin = computed(() => {
    return LoginName.value !== "";
  });

  const login = (userName: string) => {
    LoginName.value = userName;
    localStorage.setItem("LoginName", userName);
  };
  const logout = () => {
    LoginName.value = "";
    localStorage.removeItem("LoginName");
  };

  const Dataload = () => {
    LoginName.value = localStorage.getItem("LoginName") || "";
  };

  return { LoginName, isLogin, login, logout, Dataload };
});
